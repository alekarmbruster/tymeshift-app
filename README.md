# Tymeshift - React code test

### Live demo -> [here](https://tymeshift-app-s6ruq.ondigitalocean.app/)

This project was initialized with `npx create-react-app tymeshift-app --template typescript`

## Automated Lint, Code Style and Formating

[Eslint](https://eslint.org) is used for checking syntax and style errors.
[Prettier](https://prettier.io/) is used for code formating
[Husky](https://typicode.github.io/husky/#/) along with [lint-staged](https://www.npmjs.com/package/lint-staged) and [pretty-quick](https://www.npmjs.com/package/pretty-quick) is used to create pre commit hook that will check and format all staged files

## Internationalization

[react-i18next](https://react.i18next.com/) is used to handle app translations

## HTTP Client

[axios](https://github.com/axios/axios) is uset to settup HTTP service and to handle HTTP requests

## State Management

[React Redux](https://react-redux.js.org/) is used to handle state management
[Redux-Saga](https://redux-saga.js.org/) is used to handle asynchronous api calls
[reselect](https://www.npmjs.com/package/reselect) is used to extract data from redux state

## Styles

[Styles components](https://styled-components.com/) is used styling

## Date Formating

[moment](https://momentjs.com/) is used to for date formating

yarn create react-app my-app --template typescript

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run lint`

Run lint check for all the issues in all the files and give you a list of warnings.

### `npm run lint-fix`

Run lint check and fix the issues as well wherever possible.

### `npm run format`

Prettier format all `js,jsx,ts,tsx,json,md,css` files inside project

## TO DO

State management needs refactoring and transition from JavaScript to TypeScript need to be implemented. Due to lack of time this transition has not yet been made.
