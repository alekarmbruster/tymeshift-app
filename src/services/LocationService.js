import ApiService from './ApiService';

const ENDPOINTS = {
  LOCATIONS: '/locations',
};

class LocationService extends ApiService {
  getLocations = () => this.apiClient.get(ENDPOINTS.LOCATIONS);
}
export const locationService = new LocationService();
