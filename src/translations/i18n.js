import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import translationEn from './locales/en.json';

const LOCALE_ENG = 'en';

export const LOCALES = [LOCALE_ENG];

export const DEFAULT_LOCALE = LOCALE_ENG;

const resources = {
  en: {
    translation: translationEn,
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: DEFAULT_LOCALE,
    fallbackLng: DEFAULT_LOCALE,
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default i18n;
