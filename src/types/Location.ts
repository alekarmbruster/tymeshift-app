export type Location = {
  createdAt: string;
  description: string;
  id: string;
  name: string;
  userCount: number;
};
