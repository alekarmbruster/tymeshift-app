import React, { Fragment, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import Header from '../components/Header';
import { getLocations, locationsSelector } from '../store/locations';
import styled from 'styled-components';
import LocationsList from '../components/LocationsList';
import { device } from '../config';
import { Location } from '../types/Location';

function LocationsPage(): React.ReactElement {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const handleGetLocations = () => dispatch(getLocations());
  const locations: Location[] = useSelector(locationsSelector);

  useEffect(() => {
    if (!locations.length) handleGetLocations();
  }, []);

  return (
    <Fragment>
      <Header category={t('header.category')} subcategory={t('header.subcategory')} />
      <Wrapper>
        <LocationsList locations={locations} />
      </Wrapper>
    </Fragment>
  );
}

export default LocationsPage;

const Wrapper = styled.section`
  margin: 25px 30px;
  @media ${device.tablet} {
    margin: 25px 60px;
  }
`;
