import React, { Dispatch, SetStateAction } from 'react';
import { useTranslation } from 'react-i18next';
import Modal from 'react-modal';
import styled from 'styled-components';
import closeIcon from '../assets/icons/Close.svg';
import Button from '../components/Button';
import LocationBasicInfo from '../components/LocationBasicInfo';
import { Location } from '../types/Location';

type Props = {
  location: Location;
  isModalVisible: boolean;
  setModalVisibility: Dispatch<SetStateAction<boolean>>;
  viewCount: number;
};

function LocationPreviewModal({ location, isModalVisible, setModalVisibility, viewCount }: Props): React.ReactElement {
  const { t } = useTranslation();

  const closeModal = () => {
    setModalVisibility(false);
  };

  return (
    <Modal isOpen={isModalVisible} onRequestClose={closeModal} style={ModalStyle}>
      <ModalHeader>
        <Title>{t('locations.cardTitle')}</Title>
        <img src={closeIcon} alt="close icon" onClick={closeModal} />
      </ModalHeader>
      <ModalBody>
        <LocationBasicInfo location={location} viewCount={viewCount} />
        <DescriptionTitle>{t('locations.description')}</DescriptionTitle>
        <Description>{location.description}</Description>
      </ModalBody>
      <ModalFooter>
        <Button color="#37B24D" label={t('common.done')} onClick={closeModal} />
      </ModalFooter>
    </Modal>
  );
}

export default LocationPreviewModal;

const ModalStyle = {
  content: {
    backgroundColor: 'white',
    width: '440px',
    maxWidth: '80%',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    transform: 'translate(-50%, -50%)',
    padding: '0 25px',
  },
};

const ModalHeader = styled.div`
  height: 56px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  img {
    cursor: pointer;
  }
`;
const Title = styled.div`
  font-weight: 600;
  font-size: 16px;
  line-height: 24px;
`;

const ModalBody = styled.div`
  margin: 10px 0;
`;

const DescriptionTitle = styled.div`
  line-height: 24px;
  font-weight: 600;
  margin-top: 20px;
  font-size: 14px;
`;

const Description = styled.div`
  line-height: 24px;
  font-size: 14px;
  margin-bottom: 15px;
`;

const ModalFooter = styled.div`
  height: 52px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;
