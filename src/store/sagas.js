import * as locationsSagas from './locations/sagas';

const sagas = {
  ...locationsSagas,
};

export default sagas;
