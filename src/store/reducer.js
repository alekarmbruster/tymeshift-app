import { combineReducers } from 'redux';
import { reducer as locationsReducer } from './locations';
import { RESET_STATE } from './shared';

const reducer = combineReducers({
  locations: locationsReducer,
});

export default function (state, action) {
  if (action.type === RESET_STATE) {
    state = undefined;
  }
  return reducer(state, action);
}
