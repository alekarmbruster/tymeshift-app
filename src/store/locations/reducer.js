import produce from 'immer';
import { SET_LOCATIONS } from './actionTypes';

const initialState = {
  locations: [],
};

function reducer(state = initialState, { type, payload }) {
  return produce(state, (draft) => {
    switch (type) {
      case SET_LOCATIONS:
        draft.locations = payload;
        break;
    }
  });
}

export default reducer;
