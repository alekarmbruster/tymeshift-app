import { call, put, takeLatest } from 'redux-saga/effects';
import { locationService } from '../../services/LocationService';
import { setLocations } from './actions';
import { GET_LOCATIONS } from './actionTypes';

function* getLocations() {
  try {
    const { data } = yield call(locationService.getLocations);
    yield put(setLocations(data));
  } catch (error) {
  } finally {
  }
}

export function* watchGetLocations() {
  yield takeLatest(GET_LOCATIONS, getLocations);
}
