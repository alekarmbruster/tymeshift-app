import { GET_LOCATIONS, SET_LOCATIONS } from './actionTypes';

export const getLocations = () => ({ type: GET_LOCATIONS });

export const setLocations = (locations) => ({
  type: SET_LOCATIONS,
  payload: locations,
});
