import { createSelector } from 'reselect';

const locationsStateSelector = (store) => store.locations;

export const locationsSelector = createSelector(locationsStateSelector, (state) => state.locations);
