import React from 'react';
import LocationsPage from './containers/LocationsPage';

function App(): React.ReactElement {
  return <LocationsPage />;
}

export default App;
