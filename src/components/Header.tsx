import React from 'react';
import styled from 'styled-components';

type Props = {
  category: string;
  subcategory: string;
};

function Header({ category, subcategory }: Props): React.ReactElement {
  return (
    <Wrapper>
      <Category>{category}</Category>
      <Subcategory>{subcategory}</Subcategory>
    </Wrapper>
  );
}

export default Header;

const Wrapper = styled.section`
  padding: 0 30px;
  border-bottom: 1px solid #eeeeee;
`;

const Category = styled.div`
  font-weight: 700;
  font-size: 16px;
  line-height: 28px;
  letter-spacing: 0.18px;
  margin-top: 19px;
  color: rgb(0, 17, 34, 0.3);
`;

const Subcategory = styled.div`
  font-weight: 600;
  font-size: 22px;
  line-height: 28px;
  margin-bottom: 21px;
`;
