import React, { Fragment, useState } from 'react';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import editIcon from '../assets/icons/Edit.svg';
import { device } from '../config';
import LocationPreviewModal from '../modals/LocationPreviewModal';
import { Location } from '../types/Location';
import LocationBasicInfo from './LocationBasicInfo';
import { isMobile } from 'react-device-detect';

type Props = {
  location: Location;
};

function LocationListItem({ location }: Props): React.ReactElement {
  const { t } = useTranslation();
  const [viewCount, setViewCount] = useState<number>(0);
  const [isLocationModalVisible, setLocationModalVisibility] = useState<boolean>(false);

  const openLocationModal = (): void => {
    setLocationModalVisibility(true);
    setViewCount(viewCount + 1);
  };

  const handleMobileClick = (): void => {
    if (isMobile) openLocationModal();
  };

  return (
    <Fragment>
      <Wrapper onClick={handleMobileClick}>
        <Title>{t('locations.cardTitle')}</Title>
        <LocationBasicInfo location={location} viewCount={viewCount} />
        <EditIcon onClick={openLocationModal}>
          <img src={editIcon} alt="edit icon" />
        </EditIcon>
      </Wrapper>
      <LocationPreviewModal
        location={location}
        viewCount={viewCount}
        isModalVisible={isLocationModalVisible}
        setModalVisibility={setLocationModalVisibility}
      />
    </Fragment>
  );
}

export default LocationListItem;

const EditIcon = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  background-color: rgb(255, 255, 255);
  height: 26px;
  width: 26px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  display: none;
  cursor: pointer;
  img {
    width: 14px;
    height: 14px;
  }
`;

const Wrapper = styled.section`
  flex: 1;
  background-color: rgba(246, 246, 246, 0.2);
  padding: 15px 25px;
  font-size: 14px;
  border: 1px solid rgba(0, 17, 34, 0.1);
  border-radius: 2px;
  position: relative;
  &:hover {
    background-color: rgb(246, 246, 246);
    ${EditIcon} {
      display: flex;
    }
  }
  @media ${device.mobileS} {
    min-width: 200px;
  }
  @media ${device.tablet} {
    min-width: 250px;
  }
`;

const Title = styled.div`
  font-weight: 700;
  font-size: 18px;
  line-height: 32px;
  margin-bottom: 5px;
`;
