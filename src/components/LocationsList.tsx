import React from 'react';
import styled from 'styled-components';
import { Location } from '../types/Location';
import LocationListItem from './LocationListItem';

type Props = {
  locations: Array<Location>;
};

function LocationsList({ locations }: Props): React.ReactElement {
  const locationsList = locations.map((location) => (
    <LocationListItem key={location.id.toString()} location={location} />
  ));

  return <Wrapper>{locationsList}</Wrapper>;
}

export default LocationsList;

const listItemsGap = '25px';

const Wrapper = styled.section`
  display: inline-flex;
  flex-wrap: wrap;
  /* approach with negative margins that will work on all browsers instead of flex gap that is not fully supported */
  margin: -${listItemsGap} 0 0 -${listItemsGap};
  width: calc(100% + ${listItemsGap});
  > * {
    margin: ${listItemsGap} 0 0 ${listItemsGap};
  }
`;
