import React from 'react';
import styled from 'styled-components';

type Props = {
  color: string;
  label: string;
  onClick(): void;
};

function Button({ color, label, onClick }: Props): React.ReactElement {
  return (
    <Wrapper color={color} onClick={onClick}>
      <ButtonLabel>{label}</ButtonLabel>
    </Wrapper>
  );
}

export default Button;

const Wrapper = styled.div`
  background-color: ${(props) => props.color};
  height: 32px;
  padding: 0 15px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border-radius: 16px;
  cursor: pointer;
`;

const ButtonLabel = styled.span`
  font-size: 14px;
  color: white;
`;
