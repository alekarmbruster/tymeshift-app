import React from 'react';
import styled from 'styled-components';
import usersIcon from '../assets/icons/Users.svg';
import timezomeIcon from '../assets/icons/Timezone.svg';
import viewIcon from '../assets/icons/Views.svg';
import { Location } from '../types/Location';
import moment from 'moment';

type Props = {
  location: Location;
  viewCount: number;
};

function LocationBasicInfo({ location, viewCount }: Props): React.ReactElement {
  return (
    <Wrapper>
      <CardRow>
        <img src={usersIcon} alt="user icon" />
        <div>{location.userCount}</div>
      </CardRow>
      <CardRow>
        <img src={timezomeIcon} alt="rimezone icon" />
        <div>{moment(location.createdAt).format('h:mma ({}Z)').replace('{}', 'GMT')}</div>
      </CardRow>
      <CardRow>
        <img src={viewIcon} alt="view icon" />
        <div>{viewCount}</div>
      </CardRow>
    </Wrapper>
  );
}

export default LocationBasicInfo;

const Wrapper = styled.section`
  font-size: 14px;
`;

const CardRow = styled.div`
  display: flex;
  align-items: center;
  line-height: 24px;
  img {
    width: 14px;
    height: 14px;
    margin-right: 8px;
  }
  div {
    font-weight: 500;
  }
`;
